using System;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Threading;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections;
using UnityEngine;

class YumiArmController
{
    TcpClient socket = new TcpClient();
    StreamWriter writer;
    StreamReader reader;

    Queue<string> inbox = new Queue<string>();
    object inboxLock = new object();
    Queue<string> outbox = new Queue<string>();
    object outboxLock = new object();

    Thread outThread;
    Thread inThread;
    bool runThreads = false;

    string address;
    int port;

    public YumiArmController(string address, int port)
    {
        this.address = address;
        this.port = port;
    }

    public void Connect()
    {
        Connect(address, port);
    }

    public void Connect(string address, int port)
    {
        AddMessage("started connection on " + address + ":" + port);
        socket.BeginConnect(address, port, new AsyncCallback(ConnectCallback), socket);
    }

    public void Disconnect()
    {
        runThreads = false;
    }

    byte[] receiveBuffer = new byte[1024];

    private void ConnectCallback(IAsyncResult ar)
    {
        TcpClient client = (TcpClient)ar.AsyncState;
        if (!client.Connected)
        {
            AddMessage("Connection failure");
            return;
        }
        AddMessage("Connection successful");
        client.EndConnect(ar);

        runThreads = true;
        outThread = new Thread(new ThreadStart(OutThread));
        outThread.Start();
        outThread.IsBackground = true;

        BeginReceiveData();
    }

    void BeginReceiveData()
    {
        socket.Client.BeginReceive(receiveBuffer, 0, receiveBuffer.Length, SocketFlags.None, EndReceiveData, null);
    }

    void EndReceiveData(System.IAsyncResult iar)
    {
        int numBytesReceived = socket.Client.EndReceive(iar);
        if (numBytesReceived > 0)
        {
            string text = System.Text.Encoding.ASCII.GetString(receiveBuffer, 0, numBytesReceived);
            AddMessage(text);
        }
        if (runThreads && socket.Connected)
        {
            BeginReceiveData();
        }
    }

    public void Send(string message)
    {
        lock (outboxLock) outbox.Enqueue(message);
    }

    public bool HasMessages()
    {
        return inbox.Count != 0;
    }

    public string GetNextMessage()
    {
        string s = null;
        lock (inboxLock)
        {
            if(inbox.Count > 0) s = inbox.Dequeue();
        }
        return s;
    }

    private void AddMessage(string message)
    {
        lock (inboxLock) inbox.Enqueue(message);
    }

    private void OutThread()
    {
        writer = new StreamWriter(socket.GetStream());

        while (runThreads)
        {
            string last = "";
            if (outbox.Count > 0)
            {
                lock (outboxLock)
                {
                    if(outbox.Count > 0)
                    {
                        last = outbox.Dequeue();
                    }
                }
                if (last.Length > 0)
                {
                    writer.WriteLine(last);
                    writer.Flush();
                }
            }
            Thread.Sleep(100);
        }
        socket.GetStream().Close();
        socket.Client.Disconnect(false);
        AddMessage("Disconnecting");
    }
}

