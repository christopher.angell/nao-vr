//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: For controlling in-game objects with tracked devices.
//
//=============================================================================

using UnityEngine;
using Valve.VR;
using System;
using System.Text;

public class SteamVR_FVSpecificTrackedObject : MonoBehaviour
{
	public enum EIndex
	{
		None = -1,
		Hmd = (int)OpenVR.k_unTrackedDeviceIndex_Hmd,
		Device1,
		Device2,
		Device3,
		Device4,
		Device5,
		Device6,
		Device7,
		Device8,
		Device9,
		Device10,
		Device11,
		Device12,
		Device13,
		Device14,
		Device15
	}

	public string UniqueID;
	public EIndex index;


	[Tooltip("If not set, relative to parent")]
	public Transform origin;

    public bool isValid { get; private set; }

	private void OnNewPoses(TrackedDevicePose_t[] poses)
	{
		if (index == EIndex.None)
			return;

		var i = (int)index;

        isValid = false;
		if (poses.Length <= i)
			return;

		if (!poses[i].bDeviceIsConnected)
			return;

		if (!poses[i].bPoseIsValid)
			return;

        isValid = true;

		var pose = new SteamVR_Utils.RigidTransform(poses[i].mDeviceToAbsoluteTracking);

		if (origin != null)
		{
			transform.position = origin.transform.TransformPoint(pose.pos);
			transform.rotation = origin.rotation * pose.rot;
		}
		else
		{
			transform.localPosition = pose.pos;
			transform.localRotation = pose.rot;
		}
	}

	SteamVR_Events.Action newPosesAction;

	SteamVR_FVSpecificTrackedObject()
	{
		newPosesAction = SteamVR_Events.NewPosesAction(OnNewPoses);
	}

	void OnEnable()
	{
		var render = SteamVR_Render.instance;
		if (render == null)
		{
			enabled = false;
			return;
		}
		newPosesAction.enabled = true;

		if (UniqueID == "") {
			foreach (int i in Enum.GetValues(typeof(EIndex))) {
				var uniqueID = getPropertyString ((uint)i, ETrackedDeviceProperty.Prop_SerialNumber_String);
				var deviceType = getPropertyString ((uint)i, ETrackedDeviceProperty.Prop_ModelNumber_String);
				if (deviceType != "")
					print (deviceType + ": " + uniqueID);
			}
		} else {
			foreach (int i in Enum.GetValues(typeof(EIndex))) {
				print (UniqueID);
				if (UniqueID == getSerial ((uint)i)) {
					this.index = (EIndex)i;
					print ("device " + UniqueID + " set to index: " + i.ToString ());
				}
			}
		}
	}

	void OnDisable()
	{
		newPosesAction.enabled = false;
		isValid = false;
	}

	string getPropertyString (uint idx, ETrackedDeviceProperty prop)
	{
		ETrackedPropertyError error = new ETrackedPropertyError();
		StringBuilder sb = new StringBuilder();
		OpenVR.System.GetStringTrackedDeviceProperty(idx, prop, sb, OpenVR.k_unMaxPropertyStringSize, ref error);
		return sb.ToString ();
	}

	string getSerial(uint idx)
	{
		ETrackedPropertyError error = new ETrackedPropertyError();
		StringBuilder sb = new StringBuilder();
		OpenVR.System.GetStringTrackedDeviceProperty(idx, ETrackedDeviceProperty.Prop_SerialNumber_String, sb, OpenVR.k_unMaxPropertyStringSize, ref error);
		var probablyUniqueDeviceSerial = sb.ToString();
		return probablyUniqueDeviceSerial;
	}

	public void SetDeviceIndex(int index)
	{
		if (System.Enum.IsDefined(typeof(EIndex), index))
			this.index = (EIndex)index;
        print("this should not be called");
	}
}

