﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Sockets;
using System.Text;
using System.Linq;
using System.Threading;

public class LaserPointer : MonoBehaviour
{

    private SteamVR_TrackedObject trackedObj;

    public GameObject laserPrefab;
    private GameObject laser;
    private Transform laserTransform;

    private Vector3 hitPoint; // variable that records where the user pointed to.

    public Transform cameraRigTransform;
    public GameObject teleportReticlePrefab;

    private GameObject reticle;
    private Transform teleportReticleTransform;

    public Transform headTransform;

    public Vector3 teleportReticleOffset;
    public LayerMask teleportMask;
    private bool shouldTeleport;

    public Transform trackerTransform;

    // UPD Send socket variable
    System.Net.Sockets.UdpClient udpClient;
    // System.Net.Sockets.UdpClient udpClientReceive;
    // System.Net.Sockets.UdpClient udp;
    // System.Net.IPEndPoint groupEP;

    // Internal flags
    private bool showLaser;
    private bool moving;
    private float naoState;
    private int movingInterval;
    private bool udpReceived;

    // UDP receive socket thread
    private Thread t;

    // UDP Receive socket variables
    private Socket conn;
    private NetworkStream stream;
    private bool openConnection = false;
    public string remoteHost = "127.0.0.1";
    public int remotePort = 5006;
    public const int messageLength = 4096;
    public int timeout = 1000;
    private byte[] rMessageHolder;
    private byte[] rLengthHolder;
    private string rMessage = "";
    private bool keepAlive = true;

    // Instantiate the controller
    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    void Start()
    {
        laser = Instantiate(laserPrefab);
        laserTransform = laser.transform;
        reticle = Instantiate(teleportReticlePrefab);
        teleportReticleTransform = reticle.transform;

        naoState = (float) 0.0; // Nao is not moving

        // UPD send connection setup
        udpClient = new System.Net.Sockets.UdpClient("127.0.0.1", 5005);

        // UDP Receiving connection setup
        rMessageHolder = new byte[messageLength]; // initialize the message holder
        rLengthHolder = new byte[4]; // initialize the length holder
        try
        {
            Debug.Log(remoteHost + ":" + remotePort);

            // Create the ipaddress to listen to.
            System.Net.IPAddress ipaddress = System.Net.IPAddress.Parse(remoteHost);
            // new System.Net.IPAddress(new byte[] { 127, 0, 0, 1 })

            // Create the connection and then bind to the connection. Binding is necessary for
            // the connection to listen to the port.
            conn = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            conn.Bind(new System.Net.IPEndPoint(ipaddress, remotePort));
            //conn.Connect(remoteHost, remotePort); // No need to connect, maybe because UDP receive not TCP.
            Debug.Log("Connection established!");
            // Set the connection timeout.
            conn.ReceiveTimeout = timeout;
            openConnection = true;
        }
        catch (System.Exception e)
        {
            Debug.LogWarning("Socket error:" + e);
        }

        // UDP receiving thread setup
        t = new Thread(new ThreadStart(ThreadReceive));
        t.IsBackground = true;
        t.Start();
        Debug.Log("threads started.");
        //

        showLaser = false;
        moving = false;
    }

    void OnApplicationQuit()
    {
        // Abort the thread on exit.
        t.Abort();
    }

    void OnDestroy()
    {
        keepAlive = false; // Terminate thread
    }


    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    public void ThreadReceive()
    {
        Debug.Log("Thread entered.");
        while (keepAlive)
        {
            try
            {
                rMessage = "";
                // UDP Recieve from Python script
                int location = conn.Receive(rMessageHolder);
                rMessage = Encoding.UTF8.GetString(rMessageHolder, 0, location);
                udpReceived = true;
                Debug.Log("Received data: " + rMessage);
            }
            catch (System.Exception e)
            {
                // This is always called on a timeout.
                Debug.Log(rMessageHolder.Length.ToString() + "Receive timeout; " + e.Message);
            }
        }
    }

    private void ShowLaser(RaycastHit hit)
    {

        laser.SetActive(true);
        laserTransform.position = Vector3.Lerp(trackedObj.transform.position, hitPoint, .5f);
        laserTransform.LookAt(hitPoint);
        laserTransform.localScale = new Vector3(laserTransform.localScale.x, laserTransform.localScale.y,
            hit.distance);
    }

    // Update is called once per frame
    void Update()
    {
        // Controller doesn't record that the trigger is still down, only that it has
        // been pressed.
        // showLaser records transition to down and up for the laser. 
        // When showLaser is true, it means trigger is still down.
        if (Controller.GetHairTriggerDown() || showLaser)
        {
            RaycastHit hit;
            showLaser = true;

            // Get the location that the controller is pointing to.
            if (Physics.Raycast(trackedObj.transform.position, transform.forward, out hit, 100, teleportMask))
            {
                hitPoint = hit.point;
                ShowLaser(hit);
            
                reticle.SetActive(true);
                teleportReticleTransform.position = hitPoint + teleportReticleOffset;
                shouldTeleport = true;
            }
        }
        else
        {
            laser.SetActive(false);
            reticle.SetActive(false);
        }

        // Signal that controller trigger is up.
        if (Controller.GetHairTriggerUp())
        {
            showLaser = false;
        }
        if (Controller.GetHairTriggerUp() && shouldTeleport)
        {
            moving = true;
            naoState = (float) 1; // Set Nao's state to walking
            Teleport(); // Start Nao walking.
        }

        // These two have become redundant.
        if (moving && naoState > 0.0)
        {
            // Don't need to check the data, just that it's there is enough to indicate that
            // a walk cycle is done.
            //Debug.Log("Getting UDP data.");

            //naoState = (float) 2.0;

            // Continue Nao walking
            Teleport();
        }
    }

    private void Teleport()
    {
        shouldTeleport = false;
        reticle.SetActive(false);

        // These three lines are for teleporting the camera position, so are not needed
        // here for nao motion.
        // Vector3 difference = cameraRigTransform.position - headTransform.position;
        // difference.y = 0;
        // cameraRigTransform.position = hitPoint + difference;

        // Get the relative position from the hit point to the tracker
        Vector3 relativePos = hitPoint - trackerTransform.position;
        // Get the y tracker orientation angle.
        float trackerAngle = trackerTransform.rotation.eulerAngles.y;

        // Draw a debug ray from the tracker position in the direction of the relative position.
        // Show for 6 seconds.
        Debug.DrawRay(trackerTransform.position, relativePos, Color.red, 6);

        // Get the angles for looking at the relative position from an absolute
        // coordinate space. This is the angle that you would look to if you were at
        // the origin. Since it's a relative position, it's the angle that you would
        // look to from the tracker in an absolute coordinate sense.
        Quaternion looktoAngles = Quaternion.LookRotation(relativePos);
        float x = looktoAngles.eulerAngles.x;
        float y = looktoAngles.eulerAngles.y;
        float z = looktoAngles.eulerAngles.z;

        //Debug.Log("x: " + x.ToString() + ", y: " + y.ToString() + ", z: " + z.ToString());

        // Nao expects positive goes to left, negative to right
        // Having the direction angle subtracted from the tracker angle accomplishes this.
        float naoAngle = trackerAngle - y; 
        if (naoAngle > (float)180.0)
        {
            naoAngle -= (float)360.0;
        }
        if (naoAngle < (float)-180.0)
        {
            naoAngle += (float)360.0;
        }

        // The initial state is 1.0, or send angle information
        // Once Nao has achieved the correct angle within +/-5 degrees, then switch
        // state to walking mode, state 2.0.
        if (naoAngle < (float)5 && naoAngle > (float)-5)
        {
            naoState = (float)2.0;
        }

        float naoDistance = Mathf.Sqrt(relativePos.x * relativePos.x + relativePos.z * relativePos.z);
        // Once Nao has reached with 5 cm of target position (0.05 m), 
        // End walking by switching to state 0.0.
        if (naoDistance < 0.05)
        {
            naoState = (float) 0.0;
            moving = false;
        }

        Debug.Log("tAngle: " + trackerAngle.ToString() + ", angle: " + naoAngle.ToString() + ", distance: " + naoDistance.ToString());

        // Send information over to Nao
        System.Byte[] sendBytes = System.BitConverter.GetBytes(naoState);
        sendBytes = sendBytes.Concat(System.BitConverter.GetBytes(naoAngle)).ToArray();
        sendBytes = sendBytes.Concat(System.BitConverter.GetBytes(naoDistance)).ToArray();
        try
        {
            udpClient.Send(sendBytes, sendBytes.Length);
        }
        catch (System.Exception e)
        {
            Debug.Log(e.ToString());
        }
    }
}
